package fr.epsi.hmf;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //Affichage TVA
        System.out.println( "Récapitulatif Code pays / TVA       Récapitulatif taux de réductions" );
        System.out.println( "-----------------------------       --------------------------------" );
        System.out.println( "Code pays                 TVA       Montant total          Réduction" );
        System.out.println( "DK                         25       >1000                         3%" );
        System.out.println( "ES                         21       >5000                         5%" );
        System.out.println( "FI                         24       >7000                         7%" );
        System.out.println( "FR                         20       >10000                       10%" );
        System.out.println( "EL                         24       >50000                       15%" );
        System.out.println( "MT                         18" );

        askUserTvaTotal();
        
    }

    public static void askUserTvaTotal(){
        //Demande prix + TVA
        Scanner sc = new Scanner(System.in);
        boolean recommence = true;
        Double total = 0.0;
        Double pourcent = 0.0;
        while(recommence){
            recommence = false;
            System.out.println( "Entrez un montant total" );
            String ret = sc.nextLine();
            try{
                total = Double.parseDouble(ret);
                if(shouldSuggest3Discount(total)){
                    System.out.println( "Le total est superieur à 1000€. Voulez-vous appliquer une remise de 3% ? O/N" );
                    ret = sc.nextLine();
                    if(ret == "o" || ret == "O"){
                        System.out.println("Remise appliquée");
                        total = total - 0.03*total;
                    }
                } 
            }catch(NumberFormatException e){
                System.out.println("Format invalide");
                recommence = true;
            }
        }
        recommence = true;
        while(recommence){
            recommence = false;
            System.out.println( "Entrez un taux de TVA (20 pour 20% par exemple)" );
            String ret = sc.nextLine();
            try{
                pourcent = Double.parseDouble(ret);
            }catch(NumberFormatException e){
                System.out.println("Format invalide");
                recommence = true;
            }
        }
        sc.close();
        
        Double fin = getSumTvaTotal(total,pourcent);
        System.out.println(fin);
    }


    public static Double getSumTvaTotal(Double tot, Double TVA){
        return tot + (TVA/100)*tot;
    }

    public static boolean shouldSuggest3Discount(Double tot){
        return tot >= 1000.0;
    }
    
}

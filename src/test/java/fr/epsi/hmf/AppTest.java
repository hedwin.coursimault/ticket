package fr.epsi.hmf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        Double res = App.getSumTvaTotal(100.0, 20.0);

        assertEquals(Double.valueOf(120), res);
    }

    @Test
    public void shouldAnswerWithFalse()
    {
        boolean fail = App.getSumTvaTotal(100.0, 28.0) == 0.0;

        assertFalse(fail);
    }

    @Test
    public void shouldSuggest3DiscountTrue()
    {
        boolean succ = App.shouldSuggest3Discount(1000.0);

        assertTrue(succ);
    }

    @Test
    public void shouldSuggest3DiscountTrue2()
    {
        boolean succ = App.shouldSuggest3Discount(18780.69);

        assertTrue(succ);
    }

    @Test
    public void shouldSuggest3DiscountFalse()
    {
        boolean fail = App.shouldSuggest3Discount(999.69);

        assertFalse(fail);
    }

    @Test
    public void shouldSuggest3DiscountFalse2()
    {
        boolean fail = App.shouldSuggest3Discount(-154888.36);

        assertFalse(fail);
    }

    @Test
    public void shouldSuggest3DiscountFalse3()
    {
        boolean fail = App.shouldSuggest3Discount(58.36);

        assertFalse(fail);
    }



}
